<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\DpdPlugin\Client;

use Nfq\DpdClient\Client as BaseClient;
use Omni\Sylius\DpdPlugin\Factory\Model\ShipmentStatusFactory;
use Omni\Sylius\DpdPlugin\Model\ShipmentStatus;
use Omni\Sylius\DpdPlugin\Request\GetShipmentStatusRequest;
use Webmozart\Assert\Assert;
use Nfq\DpdClient\HttpClient\Client as HttpClient;

class Client extends BaseClient
{
    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var HttpClient
     */
    private $statusClient;

    /**
     * @param HttpClient $httpClient
     * @param HttpClient $statusClient
     */
    public function __construct(HttpClient $httpClient, HttpClient $statusClient)
    {
        $this->httpClient = $httpClient;
        $this->statusClient = $statusClient;
        parent::__construct($httpClient, $statusClient);
    }

    /**
     * @param GetShipmentStatusRequest $request
     * @return ShipmentStatus
     */
    public function getShipmentStatus(GetShipmentStatusRequest $request): ShipmentStatus
    {
        $response = $this->httpClient->post('/ws-mapper-rest/parcelStatus_', $request->toArray());
        Assert::isArray($response);

        return ShipmentStatusFactory::fromResponse($response);
    }

    /**
     * @param array $response
     */
    private function throwException(array $response)
    {
        throw new InvalidResponseException('Response is missing required information: ' . json_encode($response));
    }
}
