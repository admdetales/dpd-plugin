<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Provider;

use Nfq\DpdClient\Request\SearchParcelMachineRequest;
use Omni\Sylius\DpdPlugin\Factory\ParcelMachineFactoryInterface;
use Omni\Sylius\DpdPlugin\HttpClient\DPDClientFactory;
use Omni\Sylius\ParcelMachinePlugin\Provider\ParcelMachineProviderInterface;

class ParcelMachineProvider implements ParcelMachineProviderInterface
{
    public const PROVIDER_CODE = 'dpd';

    /**
     * @var DPDClientFactory
     */
    private $dpdFactory;

    /**
     * @var ParcelMachineFactoryInterface
     */
    private $factory;

    /**
     * @var string[]
     */
    private $countries;

    /**
     * ParcelMachineProvider constructor.
     * @param DPDClientFactory $dpdFactory
     * @param ParcelMachineFactoryInterface $factory
     * @param string[] $countries
     */
    public function __construct(DPDClientFactory $dpdFactory, ParcelMachineFactoryInterface $factory, array $countries)
    {
        $this->dpdFactory = $dpdFactory;
        $this->factory = $factory;
        $this->countries = $countries;
    }

    public function getAll(): array
    {
        $parcelMachines = [];

        foreach ($this->countries as $country) {
            $parcelMachines = array_merge($parcelMachines, $this->getByCountry($country));
        }

        return $parcelMachines;
    }

    /**
     * @param string $code
     * @return array
     * @throws \Nfq\DpdClient\Exception\InvalidResponseException
     */
    public function getByCountry(string $code): array
    {
        $client = $this->dpdFactory->create();
        $parcelMachines = $client->getParcelMachines(new SearchParcelMachineRequest($code));

        return $this->factory->createMany($parcelMachines);
    }

    public function getCode(): string
    {
        return self::PROVIDER_CODE;
    }
}
