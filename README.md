# OmniSyliusDpdPlugin

## Installation

* Run `composer require omni/sylius-dpd-plugin:dev-master php-http/guzzle6-adapter`.

* Add the following bundles to `AppKernel.php`:

```php
new Http\HttplugBundle\HttplugBundle(),
new Omni\Plugin\SyliusDpdPlugin\OmniSyliusDpdPlugin(),
```

* Optionally install `omni/sylius-parcel-machine-plugin` package for parcel machine information synchronization.

## Configuration reference

```yaml
omni_sylius_dpd:
    client_factory: httplug.factory.guzzle6
    username: '%dpd.username%'
    password: '%dpd.password%'
    test_mode: true
    parcel_machine:
        enabled_countries:
            - LT
            - LV
            - EE
```

### Running plugin tests

```bash
$ bin/phpunit
```
